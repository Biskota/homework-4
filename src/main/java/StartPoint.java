import methods.Tasks;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class StartPoint {
    private static final Logger log = Logger.getLogger(StartPoint.class);

    public static void main(String[] args) {
        Tasks tasks = new Tasks();
        Random random = new Random();
        List<String> collection = asList("aaaaa", "bbb", "a", "c", "aaa", "abb");
        List<Integer> a = random.ints(-100, 100).limit(10).boxed().collect(Collectors.toList());


        log.info("Array list: " + a);
        log.info("Add letter before number: " + tasks.getString(a));
        log.info("Sum of numbers on module more than 50: " + tasks.amountSum(a));
        log.info("List of strings: " + collection);
        log.info("Filtered list: " + tasks.listOfStrings(collection));
    }
}
