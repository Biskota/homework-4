package methods;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class Tasks {

    /**
     * @param list a list of Strings
     * @return a list of all strings that start with the letter 'a' (lower case) and have exactly 3 letters
     */
    public List<String> listOfStrings(List<String> list) {
        return list.stream()
                .filter(s -> s.startsWith("a"))
                .filter(s -> s.length() == 3)
                .collect(Collectors.toList());

    }

    /**
     * Each element should be preceded by the letter 'e' if the number is even, and preceded by the letter 'o'
     * if the number is odd. For example, if the input list is (3,44), the output should be 'o3,e44‘
     *
     * @param list list of integers.
     * @return a comma separated string based on a given list of integers.
     */
    public String getString(List<Integer> list) {
        return list.stream()
                .map(i -> i % 2 == 0 ? "e" + i : "o" + i)
                .collect(joining(","));
    }

    /**
     * @param list list with random numbers from -100 to 100.
     * @return count amount of numbers which ON MODULE more than 50.
     */
    public long amountSum(List<Integer> list) {
        return list.stream().filter(i -> Math.abs(i) > 50).count();
    }
}